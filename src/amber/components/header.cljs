(ns amber.components.header
  (:require [syn-antd.typography :as typography]
            [syn-antd.layout :as layout]
            [syn-antd.input :as input]
            [amber.components.books :as books]))

(defn header []
  [layout/layout-header {:id "header"}
   [typography/typography-title {:id "title-text"}
    "Amber.mu"]
   [input/input
    {:id "main-search-bar"
     :size "large" 
     :placeholder "Pesquise nome, autores, etc..."
     :value @books/title-pattern
     :on-change #(reset! books/title-pattern (-> % .-target .-value))}]])