(ns amber.components.books
  (:require [reagent.core :as r]
            [amber.mock.books :as books]
            [syn-antd.card :as card]
            [syn-antd.layout :as layout]
            [syn-antd.statistic :as statistic]
            [syn-antd.icon :as icon]
            [clojure.string :as s]))

(def title-pattern (r/atom nil))

(defn book [book]
  [card/card {:class "book"
              :title (:title book)
              :hoverable true
              :key (str (:isbn book) "_" (:title book))}
   [:div.book-cover [:img {:alt (:title book) :src (:thumbnail book)}]]
   [:div.book-description
    [:div [:b "Authors: "]    (first (:authors book))]
    [:div [:b "ISBN: "]       (:isbn book)]
    [:div [:b "Pages: "]      (:pageCount book)]
    [:div [:b "Categories: "] (s/join ", " (:categories book))]]
   [statistic/statistic 
    {:class "price-info"
     :title "Variação"
     :value 11.1389
     :precision 1
     :value-style {:color "#3f8600"}
     :prefix (r/as-element [icon/icon {:type "arrow-up"}])
     :suffix "%"}]])

(defn filter-books [pattern book]
  (or (= pattern nil)
      (s/includes? (s/lower-case (:title book)) (s/lower-case pattern)))) 

(defn books []
  (let [filtered-books (->> @books/books
                            (filter #(filter-books @title-pattern %1))
                            (sort-by :pageCount >))]
    [layout/layout-content
     [:div.books {:id "book-shelf"}
      (for [record filtered-books]
        (book record))]]))