(ns amber.core
  (:require [reagent.core :as reagent]
            [syn-antd.layout :as layout]
            [amber.components.header :refer [header]]
            [amber.components.books  :refer [books]]))

(defn amber []
  [layout/layout
   [header]
   [books]])

(defn start []
  (reagent/render-component [amber]
                            (. js/document (getElementById "app"))))

(defn ^:export init []
  (start))

(defn stop []
  (js/console.log "stop"))
